import type { NextPage } from 'next'
import Head from 'next/head'
import Clock from '../components/clock'
import styles from '../styles/Home.module.css'
import d0Image from '../public/image-digits/0.png'
import d1Image from '../public/image-digits/1.png'
import d2Image from '../public/image-digits/2.png'
import d3Image from '../public/image-digits/3.png'
import d4Image from '../public/image-digits/4.png'
import d5Image from '../public/image-digits/5.png'

const Home: NextPage = () => {
  return (
    <div className={styles.container}>
      <Head>
        <title>Guybrush Clock</title>
        <meta name="description" content="Guybrush Clock" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <Clock
          digitImages={[
            d0Image,
            d1Image,
            d2Image,
            d3Image,
            d4Image,
            d5Image,
          ]}
        />
      </main>
    </div>
  )
}

export default Home
