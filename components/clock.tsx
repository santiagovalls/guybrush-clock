import Image from "next/image";
import { useEffect, useState } from "react";

type Props = {
    digitImages: StaticImageData[],
}

const Clock = ({
    digitImages
}: Props) => {
    const getImageFromDigit = (digit: string) => digitImages[+digit]

    const base = digitImages.length;

    const getBaseNowTimestamp = (base: number) => (getNowTimestamp().toString(base))

    const getNowTimestamp = () => (Math.floor(Date.now() / 1000))

    const [timestamp, setTimestamp] = useState(getBaseNowTimestamp(base));

    useEffect(() => {
        const clockEngine = setInterval(() => {
            setTimestamp(getBaseNowTimestamp(base));
        }, 1000);
        return () => clearTimeout(clockEngine);
    }, []);

    return (
        <div>
            {
                timestamp.split('').map((digit, index) => {
                    return <Image
                        key={index}
                        src={getImageFromDigit(digit)}
                        width="60"
                        height="75"
                        alt={digit} />
                })
            }
        </div>
    );
}

export default Clock;